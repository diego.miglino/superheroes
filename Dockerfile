FROM openjdk:11.0.7-jre-slim
MAINTAINER diego.miglino
EXPOSE 8080
ARG JAR_FILE=target/*.war
COPY ${JAR_FILE} superheroes.war
ENTRYPOINT ["java","-jar","/superheroes.war"]