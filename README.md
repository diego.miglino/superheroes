# **API para el mantenimiento de súperhéroes.**

## Comandos para ejecutar el proyecto via Docker:
- mvn clean install -Dmaven.test.skip=true
- sudo docker build -t superheroes .
- sudo docker run -p 8080:8080 superheroes -d

>Se proporciona además el ejecutable **start.sh** que contiene los 3 comandos mencionados.

## Acceso a la base de datos (h2)
Luego de levantar la aplicación, podemos acceder a la base de datos desde la siguiente url:

[Link][1]
⋮
[1]: http://localhost:8080/h2-console/

Se podrá ingresar con los siguientes datos:
- **User name**: *sa*
- **Password**:
- **Url**: *jdbc:h2:mem:mindataDB*

> Se utilizó la herramienta **Flyway** para gestionar los scripts de base de datos.

## Documentación de la API Rest

Luego de levantar la aplicación, podemos acceder a la documentación generada en **Swagger** desde la siguiente url:

[Link][2]
⋮
[2]: http://localhost:8080/swagger-ui.html

> Se disponibiliza la colección **Postman** para facilitar las pruebas. 

> Es necesario crear una variable de entorno en Postman llamada **"Access_Token"** para reutilizar el token generado.

## Generación de Access Token

> curl --location --request POST 'http://localhost:8080/auth/token?client_id=PhA2vqPC5n4RdsrESV6BthTLnm2d74aV&client_secret=9WfKy7K8Eqkp6Y2s2HAjLxTsKpvHL7pN' --header 'Cookie: JSESSIONID=E4FBE3D00B15C7212DA8D896B8A2EF0F'

Realizar el curl con los siguientes datos:

- **Método**: *POST*
- **Url**: *http://localhost:8080/auth/token*
- **client_id**: *PhA2vqPC5n4RdsrESV6BthTLnm2d74aV*
- **client_secret**: *9WfKy7K8Eqkp6Y2s2HAjLxTsKpvHL7pN*

Ejemplo de respuesta:
> "access__token": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzdXBlcmhlcm9lc0pXVCIsInN1YiI6IlBoQTJ2cVBDNW40UmRzckVTVjZCdGhUTG5tMmQ3NGFWIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTYyNzM2MzE1MywiZXhwIjoxNjI3MzYzNDUzfQ.tduaxpUDNHg486dv_--4TgLckwJaYLWB-hs6P14Q2-6LI_VZlaFDQHsMCgAu_rlY_1PvuEjWDsLE0SYhjDU9pg",
    "expires_in": "300 segundos",
    "token_type": "bearer"

> El token esta configurado con una duración de 5 minutos. Pasado ese tiempo, es necesario generar uno nuevo.

## CRUD Rest Endpoints

> Los resultados quedan **cacheados** en memoria hasta que se cree, actualice o elimine un superheroe.

> Cada endpoint loguea por consola los datos de entrada y los de salida, asi como el tiempo de ejecucion, mediante **anotaciones personalizadas**.

**GET - findAll**

curl --location --request **GET 'http://localhost:8080/api/superheroes'** 
--header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzdXBlcmhlcm9lc0pXVCIsInN1YiI6IlBoQTJ2cVBDNW40UmRzckVTVjZCdGhUTG5tMmQ3NGFWIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTYyNzM2MzE1MywiZXhwIjoxNjI3MzYzNDUzfQ.tduaxpUDNHg486dv_--4TgLckwJaYLWB-hs6P14Q2-6LI_VZlaFDQHsMCgAu_rlY_1PvuEjWDsLE0SYhjDU9pg' 
--header 'Cookie: JSESSIONID=9A6D0E88F8322B2E8E54D209F1477B93'

**GET - findByNameLike**

curl --location --request **GET 'http://localhost:8080/api/superheroes?name=Goku'** 
--header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzdXBlcmhlcm9lc0pXVCIsInN1YiI6IlBoQTJ2cVBDNW40UmRzckVTVjZCdGhUTG5tMmQ3NGFWIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTYyNzM2MzE1MywiZXhwIjoxNjI3MzYzNDUzfQ.tduaxpUDNHg486dv_--4TgLckwJaYLWB-hs6P14Q2-6LI_VZlaFDQHsMCgAu_rlY_1PvuEjWDsLE0SYhjDU9pg' 
--header 'Cookie: JSESSIONID=9A6D0E88F8322B2E8E54D209F1477B93'

**GET - findById**

curl --location --request **GET 'http://localhost:8080/api/superheroes/2'** 
--header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzdXBlcmhlcm9lc0pXVCIsInN1YiI6IlBoQTJ2cVBDNW40UmRzckVTVjZCdGhUTG5tMmQ3NGFWIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTYyNzM2MzE1MywiZXhwIjoxNjI3MzYzNDUzfQ.tduaxpUDNHg486dv_--4TgLckwJaYLWB-hs6P14Q2-6LI_VZlaFDQHsMCgAu_rlY_1PvuEjWDsLE0SYhjDU9pg' 
--header 'Cookie: JSESSIONID=9A6D0E88F8322B2E8E54D209F1477B93'

**POST - create superheroe**

curl --location --request **POST 'http://localhost:8080/api/superheroes'** 
--header 'Content-Type: application/json; charset=utf-8' 
--header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzdXBlcmhlcm9lc0pXVCIsInN1YiI6IlBoQTJ2cVBDNW40UmRzckVTVjZCdGhUTG5tMmQ3NGFWIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTYyNzM2MzE1MywiZXhwIjoxNjI3MzYzNDUzfQ.tduaxpUDNHg486dv_--4TgLckwJaYLWB-hs6P14Q2-6LI_VZlaFDQHsMCgAu_rlY_1PvuEjWDsLE0SYhjDU9pg' 
--header 'Cookie: JSESSIONID=9A6D0E88F8322B2E8E54D209F1477B93' 
--data-raw '{
  "name": "Vegeta",
  "force": 98,
  "velocity": 60,
  "resistance": 80,
  "energy": 90
}'

**PATCH - update superheroe**

curl --location --request **PATCH 'http://localhost:8080/api/superheroes'** 
--header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzdXBlcmhlcm9lc0pXVCIsInN1YiI6IlBoQTJ2cVBDNW40UmRzckVTVjZCdGhUTG5tMmQ3NGFWIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTYyNzM2MzE1MywiZXhwIjoxNjI3MzYzNDUzfQ.tduaxpUDNHg486dv_--4TgLckwJaYLWB-hs6P14Q2-6LI_VZlaFDQHsMCgAu_rlY_1PvuEjWDsLE0SYhjDU9pg' 
--header 'Content-Type: application/json' 
--header 'Cookie: JSESSIONID=9A6D0E88F8322B2E8E54D209F1477B93' 
--data-raw '{
  "id": 1,
  "name": "Goku SSJ",
  "force": 99,
  "velocity": 99,
  "resistance": 89,
  "energy": 99
}'

**DELETE - remove by id**

curl --location --request DELETE 'http://localhost:8080/api/superheroes/2' 
--header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzdXBlcmhlcm9lc0pXVCIsInN1YiI6IlBoQTJ2cVBDNW40UmRzckVTVjZCdGhUTG5tMmQ3NGFWIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTYyNzM2MzE1MywiZXhwIjoxNjI3MzYzNDUzfQ.tduaxpUDNHg486dv_--4TgLckwJaYLWB-hs6P14Q2-6LI_VZlaFDQHsMCgAu_rlY_1PvuEjWDsLE0SYhjDU9pg' 
--header 'Cookie: JSESSIONID=9A6D0E88F8322B2E8E54D209F1477B93'

## ABM Visual
> Se creó un ABM simple con Thymeleaf.

> Se deshabilitó spring security para facilitar el uso y las pruebas de las paginas creadas.

[Link][1]
⋮
[1]: http://localhost:8080/superheroes

## Tests Unitarios y de Integración

El proyecto cuenta con una bateria de 25 tests. Para ejecutarlos utilizar el siguiente comando:

> mvn clean test