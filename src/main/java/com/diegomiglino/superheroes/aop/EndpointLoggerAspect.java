package com.diegomiglino.superheroes.aop;

import com.diegomiglino.superheroes.controller.SuperheroesController;
import com.diegomiglino.superheroes.repository.SuperheroesRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class EndpointLoggerAspect {

  static final Logger LOGGER = LogManager.getLogger(SuperheroesController.class.getName());
  
	@Around("@annotation(EndpointInputOuputLog)")
	public Object logInputOutput(ProceedingJoinPoint joinPoint) throws Throwable {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(joinPoint.getArgs());
		System.out.println("--------------------------------------------------");
		LOGGER.info("\nINPUT to " + joinPoint.getSignature() + ".\nINPUT Args/Body: " + json);
	    @SuppressWarnings("rawtypes")
		ResponseEntity response = (ResponseEntity) joinPoint.proceed();
		json = ow.writeValueAsString(response.getBody());
		LOGGER.info("\nOUTPUT Status: " + response.getStatusCode() + ".\nOUTPUT Resp/Body: " + json);
	    return response;
	}   
	
	@Around("@annotation(EndpointExecutionTimeLog)")
	public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
	    long start = System.currentTimeMillis();
	    @SuppressWarnings("rawtypes")
		ResponseEntity response = (ResponseEntity) joinPoint.proceed();
	    long executionTime = System.currentTimeMillis() - start;
		LOGGER.info("\n>> Api Call executed in " + executionTime + "ms.");
	    return response;
	}
	
}