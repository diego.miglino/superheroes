package com.diegomiglino.superheroes.controller;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.diegomiglino.superheroes.security.AccessToken;
import com.diegomiglino.superheroes.service.AuthenticationService;
import com.diegomiglino.superheroes.utils.ErrorResponse;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("auth")
public class AuthenticationController {

	@Autowired
	AuthenticationService service;

	@ApiOperation(notes = "Obtains an Access Token to use the API", value = "token")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok", response = AccessToken.class),
			@ApiResponse(code = 401, message = "Invalid Credentials", response = ErrorResponse.class)})
	@ResponseStatus(value = HttpStatus.OK)
	@PostMapping("token")
	public ResponseEntity<Object> token(@RequestParam("client_id") String cliendId, @RequestParam("client_secret") String secret) {
		try {
			service.validateAutentication(cliendId, secret);
			String token = getJWTToken(cliendId, secret);
			AccessToken accessToken = new AccessToken();
			accessToken.setAccessToken(token);
			accessToken.setExpiresIn("300 segundos");
			accessToken.setTokenType("bearer");;
			return ResponseEntity.ok(accessToken);
		} catch (AuthenticationException e) {
			Map<String, String> params = new HashMap<String, String>();
			params.put("client_id", cliendId);
			params.put("client_secret", secret);
			ErrorResponse errorResp = new ErrorResponse("com.diegomiglino.superheroes.invalid_credentials", e.getMessage(), params);
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(errorResp);
		} catch (Exception e) {
			ErrorResponse errorResp = new ErrorResponse(e);
			return ResponseEntity.internalServerError().body(errorResp);
		}
	}

	private String getJWTToken(String cliendId, String secretKey) {
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("superheroesJWT")
				.setSubject(cliendId)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 300000)) // 5 min
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return "Bearer " + token;
	}

}