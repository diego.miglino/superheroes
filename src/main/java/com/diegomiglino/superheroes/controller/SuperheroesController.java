package com.diegomiglino.superheroes.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.diegomiglino.superheroes.entities.Superheroe;
import com.diegomiglino.superheroes.exception.SuperheroesException;
import com.diegomiglino.superheroes.service.SuperheroesService;

@Controller
@RequestMapping("superheroes")
public class SuperheroesController {

	@Autowired
	private SuperheroesService superheroesService;
	
	@GetMapping()
	public String index(Model model) {
	    return "redirect:/superheroes/list";
	}
	
	@GetMapping("/list")
    public String showList(Model model) {
		List<Superheroe> result = superheroesService.findAll();
	    model.addAttribute("superheroes", result);
	    return "list";	
	}
	  
	@GetMapping("/create")
    public String showCreateForm(Model model) {
	    model.addAttribute("heroe", new Superheroe());
        return "create";
    }

	@PostMapping("/save")
	public String create(Superheroe heroe, RedirectAttributes redirectAttributes, Model model) {
		try {
			superheroesService.createSuperheroe(heroe);
		    return "redirect:/superheroes/list";
		} catch (SuperheroesException e) {
			redirectAttributes.addFlashAttribute("errorMessage", e.errorMessage());
	        return "redirect:/superheroes/create	";
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
	        return "redirect:/superheroes/create";
		}
	}
	
	@GetMapping("/edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		Superheroe superheroe = superheroesService.findById(id);
	    model.addAttribute("heroe", superheroe);
	    return "edit";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") long id, Superheroe heroe, RedirectAttributes redirectAttributes, Model model) {
		try {
			superheroesService.updateSuperheroe(heroe);
		    return "redirect:/superheroes/list";
		} catch (SuperheroesException e) {
			redirectAttributes.addFlashAttribute("errorMessage", e.errorMessage());
	        return "redirect:/superheroes/edit/" + heroe.getId();
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
	        return "redirect:/superheroes/edit/" + heroe.getId();
		}
	}

	@GetMapping("/delete/{id}")
	public String removeById(@PathVariable("id") long id, Model model) {
		superheroesService.removeById(id);
	    return "redirect:/superheroes/list";
	}

}