package com.diegomiglino.superheroes.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.diegomiglino.superheroes.aop.EndpointExecutionTimeLog;
import com.diegomiglino.superheroes.aop.EndpointInputOuputLog;
import com.diegomiglino.superheroes.entities.Superheroe;
import com.diegomiglino.superheroes.exception.SuperheroesException;
import com.diegomiglino.superheroes.service.SuperheroesService;
import com.diegomiglino.superheroes.utils.ErrorResponse;
import com.diegomiglino.superheroes.utils.SuperheroeAttributeEnum;
import com.diegomiglino.superheroes.utils.ValidationUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("api/superheroes")
public class SuperheroesRestController {

	@Autowired
	private SuperheroesService superheroesService;

	@GetMapping("/{id}")
	@EndpointInputOuputLog
	@EndpointExecutionTimeLog
	public ResponseEntity<Superheroe> findById(
			@ApiParam(name = "id", type = "Long", value = "Id of the Superheroe we want to find.", example = "1", required = true) @PathVariable Long id) {
		Superheroe sh = superheroesService.findById(id);
		return ResponseEntity.ok(sh);
	}

	@ApiOperation(notes = "If name param is empty, then all Superheroes will be returned.", value = "findAll")
	@RequestMapping(method = RequestMethod.GET)
	@EndpointInputOuputLog
	@EndpointExecutionTimeLog
	public ResponseEntity<List<Superheroe>> findByName(
			@ApiParam(name = "name", type = "String", value = "Matches all Superheroes containing this value on his name.", example = "Goku", required = false) @RequestParam(value = "name", required = false) String name) {
		
		List<Superheroe> result = new ArrayList<>();

		if (name != null)
			result = superheroesService.findContainingName(name);
		else
			result = superheroesService.findAll();

		return ResponseEntity.ok(result);
	}

	@ApiOperation(notes = "Creates a Superheroe with the supplied values", value = "create")
	@ApiResponses(value = { 
			@ApiResponse(code = 201, message = "Created", response = Superheroe.class),
			@ApiResponse(code = 400, message = "Validation Error", response = ErrorResponse.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
	@ResponseStatus(value = HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@EndpointInputOuputLog
	@EndpointExecutionTimeLog
	public ResponseEntity<Object> create(@RequestBody Superheroe heroe) {
		try {
			String[] requiredParams = new String[5];
			requiredParams[0] = SuperheroeAttributeEnum.NAME.toString();
			requiredParams[1] = SuperheroeAttributeEnum.FORCE.toString();
			requiredParams[2] = SuperheroeAttributeEnum.ENERGY.toString();
			requiredParams[3] = SuperheroeAttributeEnum.VELOCITY.toString();
			requiredParams[4] = SuperheroeAttributeEnum.RESISTANCE.toString();
			ValidationUtil.validateRequiredParams(heroe, requiredParams);
			superheroesService.createSuperheroe(heroe);
			return ResponseEntity.created(null).body(heroe); // Superheroe is returned to show the inserted ID 
		} catch (SuperheroesException e) {
			ErrorResponse errorResp = new ErrorResponse(e);
			return ResponseEntity.badRequest().body(errorResp);
		} catch (Exception e) {
			ErrorResponse errorResp = new ErrorResponse(e);
			return ResponseEntity.internalServerError().body(errorResp);
		}
	}

	@ApiOperation(notes = "Updates the Superheroe values based on the ID supplied", value = "update")
	@ApiResponses(value = { 
			@ApiResponse(code = 202, message = "Updated", response = Void.class),
			@ApiResponse(code = 400, message = "Validation Error", response = ErrorResponse.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
	@ResponseStatus(value = HttpStatus.ACCEPTED)
	@RequestMapping(method = RequestMethod.PATCH)
	@EndpointInputOuputLog
	@EndpointExecutionTimeLog
	public ResponseEntity<Object> update(@RequestBody Superheroe heroe) {
		try {
			String[] requiredParams = new String[1];
			requiredParams[0] = SuperheroeAttributeEnum.ID.toString();
			ValidationUtil.validateRequiredParams(heroe, requiredParams);
			superheroesService.updateSuperheroe(heroe);
			return ResponseEntity.accepted().build();
		} catch (SuperheroesException e) {
			ErrorResponse errorResp = new ErrorResponse(e);
			return ResponseEntity.badRequest().body(errorResp);
		} catch (Exception e) {
			ErrorResponse errorResp = new ErrorResponse(e);
			return ResponseEntity.internalServerError().body(errorResp);
		}
	}

	@ApiOperation(notes = "Removes the Superheroe based on the ID supplied", value = "remove")
	@ApiResponses(value = { 
			@ApiResponse(code = 204, message = "Removed", response = Void.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@DeleteMapping("/{id}")
	@EndpointInputOuputLog
	@EndpointExecutionTimeLog
	public ResponseEntity<Object> removeById(
			@ApiParam(name = "id", type = "Long", value = "ID of the Superheroe we want to remove.", example = "1", required = true) 
			@PathVariable Long id) {
		
		try {
			superheroesService.removeById(id);
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			ErrorResponse errorResp = new ErrorResponse(e);
			return ResponseEntity.internalServerError().body(errorResp);
		}
	}
}