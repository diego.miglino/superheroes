package com.diegomiglino.superheroes.exception;

public class ErrorCodes {

    public static final String MISSING_PARAMETERS = "com.diegomiglino.superheroes.error.missing_required_parameter";
    public static final String DUPLICATED_NAME = "com.diegomiglino.superheroes.error.duplicated_name";
    public static final String TOO_HIGH_VALUES = "om.diegomiglino.superheroes.error.too_high_value";
    public static final String HEROE_NOT_FOUND = "om.diegomiglino.superheroes.error.heroe_not_found";

}