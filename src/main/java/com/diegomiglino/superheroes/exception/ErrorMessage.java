package com.diegomiglino.superheroes.exception;

public class ErrorMessage {

    public static final String MSG_MISSING_PARAMETERS = "No se han informado los parámetros requeridos";
    public static final String MSG_DUPLICATED_NAME = "Ya existe un Superheroe con el nombre indicado";
    public static final String MSG_TOO_HIGH_VALUES = "Los valores de los atributos del superheroe no pueden ser mayores a 99";
    public static final String MSG_HEROE_NOT_FOUND = "No se ha encontrado el superheroe para el ID indicado";

}