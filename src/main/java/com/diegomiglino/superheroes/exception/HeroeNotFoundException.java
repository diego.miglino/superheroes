package com.diegomiglino.superheroes.exception;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Map;

public class HeroeNotFoundException extends SuperheroesException {

	private static final long serialVersionUID = 4231692992208664729L;

	public HeroeNotFoundException(String errorCode, String errorMessage, Map<String, Long> data) {
		final ObjectNode args = new ObjectNode(JsonNodeFactory.instance);

		if(data != null)
			data.entrySet().forEach(map -> args.put(map.getKey(), map.getValue().toString()));

		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.args = args;
	}
}
