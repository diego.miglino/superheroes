package com.diegomiglino.superheroes.exception;

import java.util.Map;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class RequiredParametersException extends SuperheroesException {
    
	private static final long serialVersionUID = 5845812544278177040L;

	public RequiredParametersException(String errorCode, String errorMessage, Map<String, String> data) {
		final ObjectNode args = new ObjectNode(JsonNodeFactory.instance);
		
		if(data != null)
			data.entrySet().forEach(map -> args.put(map.getKey(), map.getValue().toString()));
		
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.args = args;
	}

}