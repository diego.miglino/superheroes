package com.diegomiglino.superheroes.exception;

import com.fasterxml.jackson.databind.node.ObjectNode;

public abstract class SuperheroesException extends RuntimeException {
    
	private static final long serialVersionUID = -2693207626088124924L;

	String errorCode;
    String errorMessage;
    ObjectNode args;
    
    public String errorCode() {
        return errorCode;
    }
    
    public String errorMessage() {
        return errorMessage;
    }
    
    public ObjectNode arguments() {
        return args;
    }

    public SuperheroesException() {
		super();
	}

    public SuperheroesException(String errorCode, String errorMessage, ObjectNode args) {
		super();
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.args = args;
    }

	@Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("SuperheroesException(")
         .append(errorCode)
         .append(", ");
        
        if (args != null) {
            s.append(args);
        }
        else {
            s.append("{}");
        }
        
        s.append(')');
        return s.toString();
    }

}