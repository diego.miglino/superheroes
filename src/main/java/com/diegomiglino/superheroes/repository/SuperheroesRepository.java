package com.diegomiglino.superheroes.repository;

import org.springframework.data.repository.CrudRepository;

import com.diegomiglino.superheroes.entities.Superheroe;

import java.util.List;

public interface SuperheroesRepository extends CrudRepository<Superheroe, Long> {

	Boolean existsByName(String name);

	List<Superheroe> findByNameContaining(String name);

    Superheroe findByNameAndIdNot(String name, Long id);
}
