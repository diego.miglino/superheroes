package com.diegomiglino.superheroes.security;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccessToken {
	
	@JsonProperty(value = "access_token")
	String accessToken;
	@JsonProperty(value = "expires_in")
	String expiresIn;
	@JsonProperty(value = "token_type")
	String tokenType;
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getExpiresIn() {
		return expiresIn;
	}
	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}
	public String getTokenType() {
		return tokenType;
	}
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

}