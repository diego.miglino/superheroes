package com.diegomiglino.superheroes.service;

public interface AuthenticationService {

	Boolean validateAutentication(String cliendId, String secret);

    String getClientSecret();
}