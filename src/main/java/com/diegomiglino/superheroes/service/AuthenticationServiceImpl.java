package com.diegomiglino.superheroes.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	@Value("${client_id}")
	protected String CLIENT_ID;
	@Value("${client_secret}")
	protected String CLIENT_SECRET;

	@Override
	public Boolean validateAutentication(String cliendId, String secret) {
		if(cliendId.equals(CLIENT_ID) && secret.equals(CLIENT_SECRET))
			return true;
		else
			throw new AuthenticationCredentialsNotFoundException("Parametros de autenticación invalidos");
	}

	@Override
	public String getClientSecret() {
		return CLIENT_SECRET;
	}

}