package com.diegomiglino.superheroes.service;

import java.util.List;

import com.diegomiglino.superheroes.entities.Superheroe;
import com.diegomiglino.superheroes.exception.HeroeNotFoundException;

public interface SuperheroesService {

	Superheroe findById(Long id);

	List<Superheroe> findAll();

	List<Superheroe> findContainingName(String name);

	Superheroe createSuperheroe(Superheroe heroe);

	Superheroe updateSuperheroe(Superheroe heroe) throws HeroeNotFoundException;

	void removeById(Long id);

	Boolean validateNotExistByName(String name);

	Boolean validateNotExistsAnotherWithSameName(Long id, String name);

	Boolean validateLevelValues(Superheroe heroe);

}