package com.diegomiglino.superheroes.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.diegomiglino.superheroes.entities.Superheroe;
import com.diegomiglino.superheroes.exception.DuplicatedNameException;
import com.diegomiglino.superheroes.exception.ErrorCodes;
import com.diegomiglino.superheroes.exception.ErrorMessage;
import com.diegomiglino.superheroes.exception.HeroeNotFoundException;
import com.diegomiglino.superheroes.repository.SuperheroesRepository;
import com.diegomiglino.superheroes.utils.ValidationUtil;

@CacheConfig(cacheNames = "superheroes")
@Service
public class SuperheroesServiceImpl implements SuperheroesService {

	@Autowired
	SuperheroesRepository repository;

	/**
	 * Find a superheroe by id in the DB
	 * @param id
	 * @return a superheroe
	 */
	@Cacheable(value = "superheroecache", key = "#id")
	@Override
	public Superheroe findById(Long id) {
		return repository.findById(id).orElse(null);
	}

	/**
	 * Find all superheroes in the DB
	 * @return list of all superheroes
	 */
	@Cacheable(value = "allsuperheroes")
	@Override
	public List<Superheroe> findAll() {
		Iterable<Superheroe> iterable = repository.findAll();
		List<Superheroe> result = new ArrayList<Superheroe>();
		iterable.forEach(result::add);
		return result;
	}

	/**
	 * Find all superheroes matching the name
	 * @param id
	 * @return list of superheroe
	 */
	@Override
	public List<Superheroe> findContainingName(String name) {
		return repository.findByNameContaining(name);
	}

	/**
	 * If the validations are OK, then a superheroe is persisted
	 * @param heroe
	 * @return
	 */
	@Caching(evict = {@CacheEvict(value = "allsuperheroes", allEntries = true),
        @CacheEvict(value = "superheroecache", key = "#heroe.id")
    })
	@Override
	public Superheroe createSuperheroe(Superheroe heroe) {
		validateNotExistByName(heroe.getName());
		validateLevelValues(heroe);
		repository.save(heroe);
		return heroe;
	}

	/**
	 * If the validations are OK, then the superheroe is updated
	 * @param heroe
	 * @return
	 */
	@Caching(evict = {@CacheEvict(value = "allsuperheroes", allEntries = true),
			@CacheEvict(value = "superheroecache", key = "#heroe.id")
	})
	@Override
	public Superheroe updateSuperheroe(Superheroe heroe) throws HeroeNotFoundException {
		Optional<Superheroe> heroeOptional = repository.findById(heroe.getId());
		if(heroeOptional.isPresent()) {
			validateNotExistsAnotherWithSameName(heroe.getId(), heroe.getName());
			validateLevelValues(heroe);
			Superheroe heroeFromDb = heroeOptional.get();
			heroeFromDb.setName(heroe.getName());
			heroeFromDb.setEnergy(heroe.getEnergy());
			heroeFromDb.setForce(heroe.getForce());
			heroeFromDb.setVelocity(heroe.getVelocity());
			heroeFromDb.setResistance(heroe.getResistance());
			repository.save(heroeFromDb);
		} else {
			Map<String, Long> errors = new HashMap<String, Long>();
			errors.put("id", heroe.getId());
			throw new HeroeNotFoundException(ErrorCodes.HEROE_NOT_FOUND, ErrorMessage.MSG_HEROE_NOT_FOUND, errors);
		}
		return heroe;
	}

	/**
	 * The superheroe is removed from the DB
	 * @param id
	 */
	@Caching(evict = {@CacheEvict(value = "allsuperheroes", allEntries = true),
        @CacheEvict(value = "superheroecache", key = "#id")
    })
	@Override
	public void removeById(Long id) {
		repository.deleteById(id);
	}

	/**
	 * Check if another superheroe with the same name exits in the DB.
	 * @param name
	 * @return boolean
	 */
	@Override
	public Boolean validateNotExistByName(String name) {
		Boolean exists = repository.existsByName(name);
		if(!exists)
			return true;
		else {
			Map<String, String> errors = new HashMap<String, String>();
			errors.put("name", name);
			throw new DuplicatedNameException(ErrorCodes.DUPLICATED_NAME, ErrorMessage.MSG_DUPLICATED_NAME, errors);
		}
	}

	/**
	 * Check if exists another entity (with != id) with the same name
	 * @param id, name
	 * @return boolean
	 */
	@Override
	public Boolean validateNotExistsAnotherWithSameName(Long id, String name) {
		Superheroe result = repository.findByNameAndIdNot(name, id);
		if(result == null)
			return true;
		else {
			Map<String, String> errors = new HashMap<String, String>();
			errors.put("id", id.toString());
			errors.put("name", name);
			throw new DuplicatedNameException(ErrorCodes.DUPLICATED_NAME, ErrorMessage.MSG_DUPLICATED_NAME, errors);
		}
	}

	/**
	 * Validates that the values of the superhero attributes are lower than the constants
	 * @param id, name
	 * @return boolean
	 */
	@Override
	public Boolean validateLevelValues(Superheroe heroe) {
		return ValidationUtil.validateLevelValues(heroe);
	}

}