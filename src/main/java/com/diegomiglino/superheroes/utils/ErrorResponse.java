package com.diegomiglino.superheroes.utils;

import java.util.Map;

import com.diegomiglino.superheroes.exception.SuperheroesException;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ErrorResponse {
	
	String errorCode;
	String errorMessage;
	ObjectNode args;

	public ObjectNode getArgs() {
		return args;
	}

	public void setArgs(ObjectNode args) {
		this.args = args;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public ErrorResponse(SuperheroesException e) {
		this.args = e.arguments();
		this.errorCode = e.errorCode();
		this.errorMessage = e.errorMessage();
	}

	public ErrorResponse(Exception e) {
		this.errorMessage = e.getMessage();
	}

	public ErrorResponse(String code, String mesaage, Map<String, String> data) {
		final ObjectNode params = new ObjectNode(JsonNodeFactory.instance);
		if(data != null)
			data.entrySet().forEach(map -> params.put(map.getKey(), map.getValue().toString()));
		this.args = params;
		this.errorCode = code;
		this.errorMessage = mesaage;
	}

}