package com.diegomiglino.superheroes.utils;

public enum SuperheroeAttributeEnum {

		ID("id"),
		NAME("name"),
		FORCE("force"),
		ENERGY("energy"),
		VELOCITY("velocity"),
		RESISTANCE("resistance");

	    private final String attribute;

	    SuperheroeAttributeEnum(final String attribute) {
	        this.attribute = attribute;
	    }

	    @Override
	    public final String toString() {
	        return attribute;
	    }
	    
}