package com.diegomiglino.superheroes.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.diegomiglino.superheroes.entities.Superheroe;
import com.diegomiglino.superheroes.exception.ErrorCodes;
import com.diegomiglino.superheroes.exception.ErrorMessage;
import com.diegomiglino.superheroes.exception.RequiredParametersException;
import com.diegomiglino.superheroes.exception.TooHighValueException;

public class ValidationUtil {

	public static Boolean validateRequiredParams(Superheroe heroe, String[] requiredParams) {
		Map<String, String> data = new TreeMap<String, String>();
		if (requiredParams == null || requiredParams.length == 0)
			return true;

		for (String field : requiredParams) {
			if (field.equals(SuperheroeAttributeEnum.ID.toString())) {
				if (heroe.getId() == null || heroe.getId() <= 0)
					data.put(SuperheroeAttributeEnum.ID.toString(), "");
			} else if (field.equals(SuperheroeAttributeEnum.NAME.toString())) {
				if (heroe.getName() == null || heroe.getName().isEmpty())
					data.put(SuperheroeAttributeEnum.NAME.toString(), "");
			} else if (field.equals(SuperheroeAttributeEnum.VELOCITY.toString())) {
				if (heroe.getVelocity() == null || heroe.getVelocity() <= 0)
					data.put(SuperheroeAttributeEnum.VELOCITY.toString(), "");
			} else if (field.equals(SuperheroeAttributeEnum.FORCE.toString())) {
				if (heroe.getForce() == null || heroe.getForce() <= 0)
					data.put(SuperheroeAttributeEnum.FORCE.toString(), "");
			} else if (field.equals(SuperheroeAttributeEnum.ENERGY.toString())) {
				if (heroe.getEnergy() == null || heroe.getEnergy() <= 0)
					data.put(SuperheroeAttributeEnum.ENERGY.toString(), "");
			} else if (field.equals(SuperheroeAttributeEnum.RESISTANCE.toString())) {
				if (heroe.getResistance() == null || heroe.getResistance() <= 0)
					data.put(SuperheroeAttributeEnum.RESISTANCE.toString(), "");
			}
		}

		if (!data.isEmpty())
			throw new RequiredParametersException(
				ErrorCodes.MISSING_PARAMETERS, ErrorMessage.MSG_MISSING_PARAMETERS, data);
		else
			return true;

	}

	private final static Integer MAX_VALUE_FOR_ATRIBUTE = 99;
	
	/**
	 * Check if the atributes of the superheroe aren't upper than 100
	 * @param heroe
	 * @return boolean
	 */
	public static Boolean validateLevelValues(Superheroe heroe) {
		Map<String, Integer> errors = new HashMap<String, Integer>();
		if(heroe.getEnergy() > MAX_VALUE_FOR_ATRIBUTE)
			errors.put(SuperheroeAttributeEnum.ENERGY.toString(), heroe.getEnergy());
		if(heroe.getForce() > MAX_VALUE_FOR_ATRIBUTE)
			errors.put(SuperheroeAttributeEnum.FORCE.toString(), heroe.getForce());
		if(heroe.getVelocity() > MAX_VALUE_FOR_ATRIBUTE)
			errors.put(SuperheroeAttributeEnum.VELOCITY.toString(), heroe.getVelocity());
		if(heroe.getResistance() > MAX_VALUE_FOR_ATRIBUTE)
			errors.put(SuperheroeAttributeEnum.RESISTANCE.toString(), heroe.getResistance());
		
		if(errors.isEmpty())
			return true;
		else {
			throw new TooHighValueException(ErrorCodes.TOO_HIGH_VALUES, ErrorMessage.MSG_TOO_HIGH_VALUES, errors);
		}
	}

}