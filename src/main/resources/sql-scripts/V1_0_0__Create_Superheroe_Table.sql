CREATE TABLE IF NOT EXISTS SUPERHEROE (
    id bigint identity NOT NULL PRIMARY KEY,
    name VARCHAR(100),
    force int,
    energy int,
    velocity int,
    resistance int
)