package com.diegomiglino.superheroes.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.diegomiglino.superheroes.entities.Superheroe;
import com.diegomiglino.superheroes.service.SuperheroesService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = SuperheroesRestController.class)
@AutoConfigureMockMvc(addFilters = false)
public class SuperheroesControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private SuperheroesService superheroesService;

	Superheroe s1 = new Superheroe("Goku", 90, 80, 95, 85);
	Superheroe s2 = new Superheroe("Vegeta", 85, 85, 90, 90);
	List<Superheroe> superList = Arrays.asList(s1, s2);

	@Test
	public void testNotFound() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/fail")
										.accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder)
				   .andExpect(status().isNotFound())
				   .andReturn();
		
		assertEquals(0, result.getResponse().getContentLength());
	}
	
	@Test
	public void testBadId() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/superheroes/badrequest")
										.accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder)
				   .andExpect(status().isBadRequest())
				   .andReturn();
		
		assertEquals(0, result.getResponse().getContentLength());
	}
	
	@Test
	public void findAll() throws Exception {

		s1.setId(1l); s2.setId(2l);
		Mockito.when(superheroesService.findAll()).thenReturn(superList);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/superheroes")
										.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder)
				   .andExpect(status().isOk())
				   .andExpect(content().contentType(MediaType.APPLICATION_JSON))
				   .andReturn();

		String expected = "[{\"id\":1,\"name\":\"Goku\",\"velocity\":90,\"force\":80,\"energy\":95,\"resistance\":85},{\"id\":2,\"name\":\"Vegeta\",\"velocity\":85,\"force\":85,\"energy\":90,\"resistance\":90}]";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}
	
	@Test
	public void findById() throws Exception {

		s1.setId(1l); s2.setId(2l);
		Mockito.when(superheroesService.findById(1l)).thenReturn(s1);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/superheroes/1")
										.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder)
				   .andExpect(status().isOk())
				   .andExpect(content().contentType(MediaType.APPLICATION_JSON))
				   .andReturn();

		String expected = "{\"id\":1,\"name\":\"Goku\",\"velocity\":90,\"force\":80,\"energy\":95,\"resistance\":85}";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}
	
	@Test
	public void findNullById() throws Exception {

		Mockito.when(superheroesService.findById(1l)).thenReturn(null);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/superheroes/1")
										.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder)
				   .andExpect(status().isOk())
				   .andReturn();

		assertEquals(0, result.getResponse().getContentLength());
	}
	
	@Test
	public void findByName() throws Exception {

		s1.setId(1l); s2.setId(2l);
		List<Superheroe> list = Arrays.asList(s2);
		Mockito.when(superheroesService.findContainingName("Veg")).thenReturn(list);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/superheroes?name=Veg")
										.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder)
				   .andExpect(status().isOk())
				   .andExpect(content().contentType(MediaType.APPLICATION_JSON))
				   .andReturn();

		String expected = "[{\"id\":2,\"name\":\"Vegeta\",\"velocity\":85,\"force\":85,\"energy\":90,\"resistance\":90}]";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

	@Test
	public void removeById() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/superheroes/1")
										.accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder)
				   .andExpect(status().isNoContent())
				   .andReturn();
	}

	@Test
	public void notAllowed() throws Exception {

		RequestBuilder requestBuilder = MockMvcRequestBuilders.patch("/api/superheroes/1")
										.accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder)
				   .andExpect(status().isMethodNotAllowed())
				   .andReturn();
	}

    @Test
    public void testPostOkParamsFormat() throws Exception {
		String body = "{\"name\":\"Goku\",\"velocity\":90,\"force\":80,\"energy\":95,\"resistance\":85}";

        Mockito.when(superheroesService.createSuperheroe(Mockito.any(Superheroe.class))).thenReturn(new Superheroe());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/superheroes")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testPostFailParamsFormat() throws Exception {
		String body = "{\"name\":\"Vegeta\",\"velocity\":\"asd\",\"force\":\"qwe\",\"energy\":\"zxc\",\"resistance\":\"qaz\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/api/superheroes")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testPatchOkParamsFormat() throws Exception {
		String body = "{\"id\":1,\"name\":\"Goku\",\"velocity\":90,\"force\":80,\"energy\":95,\"resistance\":85}";

        Mockito.when(superheroesService.createSuperheroe(Mockito.any(Superheroe.class))).thenReturn(new Superheroe());

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/superheroes")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted());
    }

    @Test
    public void testPatchFailParamsFormat() throws Exception {
		String body = "{\"id\":2,\"name\":\"Vegeta\",\"velocity\":\"asd\",\"force\":\"qwe\",\"energy\":\"zxc\",\"resistance\":\"qaz\"}";

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/superheroes")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testPostRequiredParams() throws Exception {
		String body = "{\"resistance\":85}";

        mockMvc.perform(MockMvcRequestBuilders.post("/api/superheroes")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()); // Validate that all fields are required
    }
    
    @Test
    public void testPatchRequiredParams() throws Exception {
		String body = "{\"name\":\"Goku\",\"velocity\":90,\"force\":80,\"energy\":95,\"resistance\":85}";

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/superheroes")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()); // Validate that 'id' field is required
    }
	
}