package com.diegomiglino.superheroes.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.diegomiglino.superheroes.entities.Superheroe;
import com.diegomiglino.superheroes.repository.SuperheroesRepository;

@DataJpaTest
@RunWith(SpringRunner.class) 
public class SuperheroesRepositoryTest {

	@Autowired
	private SuperheroesRepository superheroesRepository;

	@Test
	public void testCreateSuperheroe() throws Exception {
		Superheroe s1 = new Superheroe("Goku", 90, 80, 95, 85);
		superheroesRepository.save(s1);
		
		assertNotNull(s1);
		assertEquals(1, s1.getId());
		assertEquals("Goku", s1.getName());

		Superheroe s2 = new Superheroe("Vegeta", 85, 85, 90, 90);
		superheroesRepository.save(s2);
		
		assertNotNull(s2);
		assertEquals(2, s2.getId());
		assertEquals("Vegeta", s2.getName());
	}

	@Test
	public void testListSuperheroes() throws Exception {
		Superheroe s1 = new Superheroe("Goku", 90, 80, 95, 85);
		superheroesRepository.save(s1);
		Superheroe s2 = new Superheroe("Vegeta", 85, 85, 90, 90);
		superheroesRepository.save(s2);
		Superheroe s3 = new Superheroe("Freezer", 95, 80, 85, 90);
		
		Iterable<Superheroe> iterable = superheroesRepository.findAll();
	    List<Superheroe> result = new ArrayList<Superheroe>();
	    iterable.forEach(result::add);

	    assertTrue(result.contains(s1));
	    assertTrue(result.contains(s2));
	    assertFalse(result.contains(s3));
	}

	@Test
	public void testFindById() throws Exception {
		Superheroe s1 = new Superheroe("Goku", 90, 80, 95, 85);
		superheroesRepository.save(s1);
		Superheroe s2 = new Superheroe("Vegeta", 85, 85, 90, 90);
		superheroesRepository.save(s2);
		
		Optional<Superheroe> optional = superheroesRepository.findById(s1.getId());
		Superheroe goku = optional.orElse(null);

		assertNotNull(goku);
		assertEquals("Goku", goku.getName());
		assertEquals(s1.getId(), goku.getId());
		assertEquals(90, goku.getVelocity());
		assertEquals(80, goku.getForce());
		assertEquals(95, goku.getEnergy());
		assertEquals(85, goku.getResistance());
	}


	@Test
	public void testRemoveById() throws Exception {
		Superheroe s1 = new Superheroe("Goku", 90, 80, 95, 85);
		superheroesRepository.save(s1);
		Superheroe s2 = new Superheroe("Vegeta", 85, 85, 90, 90);
		superheroesRepository.save(s2);
		
		Optional<Superheroe> optional = superheroesRepository.findById(s1.getId());
		Superheroe goku = optional.orElse(null);

		assertNotNull(goku);
		assertEquals("Goku", goku.getName());
		assertEquals(s1.getId(), goku.getId());
		
		superheroesRepository.delete(s1);
		
		optional = superheroesRepository.findById(s1.getId());
		goku = optional.orElse(null);
		
		assertNull(goku);
	}
}