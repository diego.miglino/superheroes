package com.diegomiglino.superheroes.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.diegomiglino.superheroes.SuperheroesApplication;
import com.diegomiglino.superheroes.entities.Superheroe;
import com.diegomiglino.superheroes.exception.DuplicatedNameException;
import com.diegomiglino.superheroes.exception.TooHighValueException;
import com.diegomiglino.superheroes.repository.SuperheroesRepository;
import com.diegomiglino.superheroes.service.SuperheroesService;

@RunWith(SpringRunner.class) 
@SpringBootTest(classes=SuperheroesApplication.class)
public class SuperheroesServiceTest {

	@MockBean
	private SuperheroesRepository repository;
	
	@Autowired
	private SuperheroesService service;

	@Test(expected = TooHighValueException.class)
	public void testValidateValuesHigherThan100() {
		Superheroe goku = new Superheroe("Goku", 190, 180, 915, 850);
		service.validateLevelValues(goku);
	}

	@Test
	public void testValidateValuesLowerThan100() {
		Superheroe goku = new Superheroe("Goku", 10, 80, 91, 50);
		assertTrue(service.validateLevelValues(goku));
	}

	@Test(expected = DuplicatedNameException.class)
	public void testValidateDuplicatedName() {
		Superheroe goku = new Superheroe("Goku", 190, 180, 915, 850);
		Mockito.when(repository.existsByName(goku.getName())).thenReturn(true);
		service.validateNotExistByName(goku.getName());
	}

	@Test
	public void testValidateNotDuplicatedName() {
		Superheroe goku = new Superheroe("Goku", 190, 180, 915, 850);
		Mockito.when(repository.existsByName(goku.getName())).thenReturn(false);
		assertTrue(service.validateNotExistByName(goku.getName()));
	}

	@Test(expected = DuplicatedNameException.class)
	public void testvalidateExistsAnotherWithSameName() {
		Mockito.when(repository.findByNameAndIdNot("Thor", 1l)).thenReturn(new Superheroe());
		assertTrue(service.validateNotExistsAnotherWithSameName(1l, "Thor"));
	}

	@Test
	public void testvalidateNotExistsAnotherWithSameName() {
		Mockito.when(repository.findByNameAndIdNot("Thor", 1l)).thenReturn(null);
		assertTrue(service.validateNotExistsAnotherWithSameName(1l, "Thor"));
	}

	@Test
	public void testCacheExecutionTimes() {
		Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
		service.findAll();
		service.findAll();
		service.findAll();
		service.findAll();
		Mockito.verify(repository, Mockito.times(1)).findAll();		
	}
	
}